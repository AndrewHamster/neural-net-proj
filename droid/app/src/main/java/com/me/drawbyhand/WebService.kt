package com.me.drawbyhand

import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface WebService {
    @Multipart
    @POST("file")
    fun getPrediction(@Part file: MultipartBody.Part): Call<Int>
}