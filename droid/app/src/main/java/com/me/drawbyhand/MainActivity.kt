package com.me.drawbyhand

import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.widget.Button
import android.widget.Toast
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    val retrofit = Retrofit.Builder()
            .baseUrl("http://192.168.0.102:5000")
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    val service = retrofit.create(WebService::class.java)

    private lateinit var drawingView: MyDrawView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        drawingView = findViewById(R.id.draw)
        val save = findViewById<Button>(R.id.predict)
        save.setOnClickListener{
            val root = Environment.getExternalStorageDirectory().toString()
            val myDir = File(root)
            myDir.mkdirs()
            val format = SimpleDateFormat("MM-dd-HHmmss")
            val file = File(myDir,"drawing" + format.format(Calendar.getInstance().time) + ".png")
            if(file.exists()) file.delete()
            try {
                val out = FileOutputStream(file)
                Bitmap.createScaledBitmap(drawingView.bitmap, 28, 28, false).next{it -> it.compress(Bitmap.CompressFormat.PNG, 100, out)}
                val data = MultipartBody.Part.createFormData("file", file.name, RequestBody.create(MediaType.parse("image/*"), file))
                val call = service.getPrediction(data)
                call.enqueue(object : Callback<Int>{
                    override fun onFailure(call: Call<Int>?, t: Throwable?) {
                        tell("Error")
                    }

                    override fun onResponse(call: Call<Int>?, response: Response<Int>?) {
                        val prediction = response?.body() ?: return
                        tell("Predicted: " + prediction)
                    }
                })
//                tell("Saved!")
//                val file = out.
                out.flush()
                out.close()
            }
            catch (e: Exception)
            {
                e.printStackTrace()
            }
        }

        val clear: Button = findViewById(R.id.clear)
        clear.setOnClickListener {
            drawingView.clear()
        }

    }

    override fun onBackPressed() {
        drawingView.clear()
    }

    fun tell(text: String) = Toast.makeText(applicationContext, text, Toast.LENGTH_LONG).show()

    infix fun <T, R> T.next(map : (T) -> R) : R = map(this)
}
