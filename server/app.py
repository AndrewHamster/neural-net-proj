from flask import Flask, jsonify, Response, request, redirect, url_for
from keras.models import load_model
from scipy.ndimage import imread
from tensorflow import get_default_graph
import json
import markdown2
import numpy as np

graph = get_default_graph()


with graph.as_default():
    model = load_model('weights.h5')
print(model.summary())
model._make_predict_function()
class Cat:
    def __init__(self, name, color):
        self.name = name
        self.color = color

app = Flask(__name__)


UPLOAD_FOLDER = '/path/to/the/uploads'
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS



@app.route('/file', methods=['GET'])
def show_load_file():
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post enctype=multipart/form-data>
      <p><input type=file name=file>
         <input type=submit value=Upload>
    </form>
    '''
@app.route('/file', methods=['POST'])
def upload_file():
    # check if the post request has the file part
    if 'file' not in request.files:
        print('No file part')
        return redirect(request.url)
    file = request.files['file']
    # if user does not select file, browser also
    # submit a empty part without filename
    if file.filename == '':
        print('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        global graph
        with graph.as_default():
            image = 255 - imread(file, flatten=True).reshape(1, 28*28)
            return str(np.argmax(model.predict(image)))
        

# @app.after_request
# def make_plaintext(request):
#     request.headers["content-type"] = "text/plain"
#     return request

@app.route("/")
def hello_world():
    return "hello, world"

@app.route("/getCat")
def get_cat():
    cat = Cat("Pussy", "Black")
    return json.dumps(cat.__dict__)

@app.route("/md")
def try_markdown():
    html = markdown2.markdown_path("run.md")
    return html

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)



