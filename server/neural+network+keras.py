from keras.models import Sequential
from keras.layers import Dense, Activation
from keras import optimizers

import numpy as np
import os
from mnist import MNIST

 #returns (inputs, labels)
def load_data():
    mndata = MNIST(os.getcwd() + '/python-mnist/data')
    images, labels = mndata.load_training()
    m_images = np.array(images).reshape(60000, 28*28) / 255
    m_labels = np.zeros((60000, 10))
    for i in range(len(labels)):
        m_labels[i,labels[i]] = 1
    return (m_images, m_labels)
    
X, y = load_data()

model = Sequential()
model.add(Dense(500, input_dim=784, activation='relu'))
model.add(Dense(10, activation='softmax'))
model.compile(loss='categorical_crossentropy', optimizer=optimizers.RMSprop())
model.summary()
model.fit(X, y, epochs=20, batch_size=256)

# from keras.models import load_model
# model = load_model('mnist-neural-net.h5')

model.predict(X[1:2])
model.save('mnist-neural-net.h5')

# testing on image
from scipy.ndimage import imread
image = imread('image2.png', flatten=True)
norm = 255 - image.reshape(1, 28*28)
np.argmax(model.predict(norm))
